# react-foundation

This is a quick start foundation for react typescript projects. Each branch offers base code for 
various scenarios.


## Project setup

1. checkout a branch that fits your needs
2. copy contents to new project directory (except `node_modules/` and `dist/`)
3. initialize git for new project
    * `git init`
    * [create new bitbucket repository](https://bitbucket.org/repo/create)
    * add bitbucket remote: `git remote add origin git@bitbucket.org:<user>>/<repository>.git`
    * do not forget to also copy `.gitignore` file
4. setup environmental variables:
    * DATABASE_URL - postgres connection string
5. commit & push to git: `git add . && git commit -m "init" && git push -u origin master`


## Project Deployment 
1. [create new heroku app](https://dashboard.heroku.com/new-app)
2. add heroku remote to git repository: `heroku git:remote -a react28`
3. deploy to heroku: `git push heroku master` (heroku builds from _master_ branch)
    * to deploy other local branch to heroku: `git push heroku test:master`


## Branches

### [client-side](../client-side)

Client-side rendering only.

* express
* node-postgres
* react
* react router


### [server-side](../server-side)

Server-side rendering.

* express
* node-postgres
* react
* react router


### [client-side-redux](../client-side-redux)

Client-side rendering only.

* express
* node-postgres
* react
* react router
* redux
* custom async middleware
* [redux dev tools](http://extension.remotedev.io/)


## Yarn Scripts

* **clean**: remove built code in `dist/`
* **build**: build client javascript and CSS bundle and transpile server-side code to javascript into `dist/`
    * **build-client**: build client javascript bundle
    * **compress-client**: compress and mangle client javascript bundle
    * **build-server**: transpile server-side code to javascript
    * **build-css**: compile SCSS into compressed CSS
* **watch**: watch and rebuild client and server-side code
    * **watch-client**: build client javascript bundle with source maps, watch for changes
    * **watch-server**: transpile server-side code to javascript, watch for changes
    * **watch-css**: compile SCSS into CSS, watch for changes
* **compile-css**: helper command for compiling SCSS into CSS
* **dev-server**: start node server to serve the app, watch for server-side code changes via nodemon
* **dev**: first execute _clean_, _build-server_ and _build-css_, then concurrently start _dev-server_ and _watch_ 
* **start**: start node server optimized for heroku to serve the app
* **postinstall**: execute _clean_ and _build_ after installing dependencies (for heroku deployment)
